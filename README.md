# SKA LOW-CSP

Prototype System Integration for the SKA LOW-CSP sub-systems.

## Documentation

All documentation for this repository can be found on [ReadTheDocs][docs].

## BinderHub

Running interactive notebooks on the LOW-CSP main branch

[![Launch Binder][binder-badge]][binder-launch]

To run interactive notebooks on a LOW-CSP feature branch

- point your browser to `http://k8s.clp.skao.int/binderhub/`
- fill in URL: `ska-telescope/ska-low-csp` and
- fill in branch name under `Git ref`, e.g. `top-194-deploy-ska-low-cbf-proc`
- click `launch` and
- wait for it to lauch;
- navigate to `notebooks/` and
- select the notebook you want to run

Note: a VPN connection to the CLP is required.

## Support

For questions or remarks related to this project, contact the TOPIC Team:

- Slack: [#help-low-csp][slack]
- [Confluence Team Page][confluence]

[binder-badge]: https://raw.githubusercontent.com/choldgraf/binderhub/0df2ffc7901dc1047e71ade456d1126ae93b4c00/binderhub/static/images/badge_logo.svg
[binder-launch]: http://k8s.clp.skao.int/binderhub/v2/gl/ska-telescope%2Fska-low-csp/HEAD
[confluence]: https://confluence.skatelescope.org/display/SE/TOPIC+Team
[docs]: https://developer.skao.int/projects/ska-low-csp/en/latest/
[slack]: https://skao.slack.com/archives/C06BJJ2RGBB

PROJECT ?= ska-low-csp

DOCS_SPHINXOPTS ?= -W

HELM_RELEASE ?= ska-low-csp
HELM_CHARTS_TO_PUBLISH ?= ska-low-csp

KUBE_APP ?= ska-low-csp
KUBE_NAMESPACE ?= ska-low-csp-dev
K8S_HELMFILE_ENV ?= ds-psi-dev
K8S_UMBRELLA_CHART_PATH ?= ./helmfile.d
K8S_USE_HELMFILE ?= true

-include .make/base.mk
-include .make/k8s.mk
-include .make/helm.mk
-include .make/python.mk
-include PrivateRules.mak

# Set parameters for notebook targets
NOTEBOOK_LINT_TARGET = notebooks
NOTEBOOK_SWITCHES_FOR_PYLINT = --disable=missing-module-docstring,import-error,fixme,duplicate-code,broad-exception-caught
PYTHON_LINE_LENGTH = 127

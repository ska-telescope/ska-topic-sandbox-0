********************************************************************************
Changelog
********************************************************************************

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_, and this project adheres to `Semantic Versioning`_.

.. warning::

    We are no longer releasing new versions going forward, so this file should be considered out of date.

.. _Keep a Changelog: https://keepachangelog.com/en/1.1.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html


================================================================================
0.6.0
================================================================================

Release date: 2024-12-16

Release Management ticket: `REL-1774`_

Fixed
-----

* `SKB-359`_: Duplicate timestamps noticed in LOW CBF correlator output
* `SKB-369`_: ska-pst configured despite wrong firmware version in configuration schema
* `SKB-450`_: Schema incompatibility when configuring PST mode on LOW CSP
* `SKB-454`_: LOW CBF Connector reports wrong version
* `SKB-463`_: ska-csp-lmc-low v0.17.0 rejects AssignResource schema from TMC
* `SKB-472`_: CPS.LMC fails to reconfigure due to ObsMode bug
* `SKB-474`_: LowCspSubarray Configure command times out before LowCbfProcessors finish programming
* `SKB-476`_: Unable to configure subarray for imaging observation without supplying timing_beams configuration
* `SKB-493`_: LowCspSubarray TANGO device does not reset obsMode after aborting
* `SKB-522`_: Delay correction error in second scan of two scan test
* `SKB-527`_: ska-csp-lmc-low v0.18.0 fails to deploy with default values
* `SKB-591`_: LOW CSP/LMC subarray is not configurable after abort command
* `SKB-597`_: PST receiver beam configuration validation error
* `SKB-619`_: Multi PST scan time out
* `SKB-639`_: When PST is configured to record data at a rate higher than the server can support, it experiences an unrecoverable error when the scan is ended

Changed
-------

Version Updates
^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-csp-lmc-low``
      - ``0.17.1``
      - ``0.19.1``
      - `Link <https://developer.skao.int/projects/ska-csp-lmc-low/en/0.19.1/CHANGELOG.html>`__
    * - ``ska-low-cbf``
      - ``0.10.0``
      - ``0.11.1``
      - `Link <https://developer.skao.int/projects/ska-low-cbf/en/0.11.1/CHANGELOG.html#id1>`__
    * - ``ska-low-cbf-conn``
      - ``0.6.0``
      - ``0.6.2``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-conn/-/blob/0.6.2/CHANGELOG.md#062>`__
    * - ``ska-low-cbf-proc``
      - ``0.14.0``
      - ``0.15.3``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/-/blob/0.15.3/CHANGELOG.md#0153>`__
    * - ``ska-tango-base``
      - ``0.4.10``
      - ``0.4.13``
      - `Link <https://gitlab.com/ska-telescope/ska-tango-charts/-/releases/0.4.13>`__
    * - ``ska-tango-util``
      - ``0.4.11``
      - ``0.4.13``
      - `Link <https://gitlab.com/ska-telescope/ska-tango-charts/-/releases/0.4.13>`__

Added
-----

- A *Monitoring* section has been added to the ``ska-low-csp`` user documentation.
  This section contains information on how to configure SKAO monitoring systems for each sub-system where applicable.

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - Version
      - Changelog

    * - ``ska-pst``
      - ``1.0.1``
      - `Link <https://gitlab.com/ska-telescope/ska-pst/-/releases/1.0.1>`__

Removed
-------

- The EDA configuration file stored in this repository is now obsolete.
  Refer to the ``ska-low-csp`` user documentation for links to EDA configuration for each sub-system.

.. _REL-1774: https://jira.skatelescope.org/browse/REL-1774
.. _SKB-359: https://jira.skatelescope.org/browse/SKB-359
.. _SKB-369: https://jira.skatelescope.org/browse/SKB-369
.. _SKB-450: https://jira.skatelescope.org/browse/SKB-450
.. _SKB-454: https://jira.skatelescope.org/browse/SKB-454
.. _SKB-463: https://jira.skatelescope.org/browse/SKB-463
.. _SKB-472: https://jira.skatelescope.org/browse/SKB-472
.. _SKB-474: https://jira.skatelescope.org/browse/SKB-474
.. _SKB-476: https://jira.skatelescope.org/browse/SKB-476
.. _SKB-493: https://jira.skatelescope.org/browse/SKB-493
.. _SKB-522: https://jira.skatelescope.org/browse/SKB-522
.. _SKB-527: https://jira.skatelescope.org/browse/SKB-527
.. _SKB-591: https://jira.skatelescope.org/browse/SKB-591
.. _SKB-597: https://jira.skatelescope.org/browse/SKB-597
.. _SKB-619: https://jira.skatelescope.org/browse/SKB-619
.. _SKB-639: https://jira.skatelescope.org/browse/SKB-639

================================================================================
0.5.0
================================================================================

Release date: 2024-08-09

Release Management ticket: `REL-1629`_

Fixed
-----

* `SKB-316`_: LOW-CBF components still configuring after subarray reports obsState READY
* `SKB-389`_: LOW CBF Processor TANGO devices report incorrect version
* `SKB-439`_: Reading the ``obsMode`` attribute on a LowCspSubarray raises an exception
* `SKB-442`_: Incorrect baseline ordering

Changed
-------

Version Updates
^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-csp-lmc-low``
      - ``0.16.2``
      - ``0.17.1``
      - `Link <https://developer.skao.int/projects/ska-csp-lmc-low/en/0.17.1/CHANGELOG.html>`__
    * - ``ska-low-cbf``
      - ``0.9.0``
      - ``0.10.0``
      - `Link <https://developer.skao.int/projects/ska-low-cbf/en/0.10.0/CHANGELOG.html>`__
    * - ``ska-low-cbf-conn``
      - ``0.5.5``
      - ``0.6.0``
      - `Link <https://developer.skao.int/projects/ska-low-cbf-conn/en/0.6.0/README.html#changelog>`__
    * - ``ska-low-cbf-proc``
      - ``0.13.0``
      - ``0.14.0``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/-/blob/0.14.0/README.md#0140>`__

.. _REL-1629: https://jira.skatelescope.org/browse/REL-1629
.. _SKB-316: https://jira.skatelescope.org/browse/SKB-316
.. _SKB-389: https://jira.skatelescope.org/browse/SKB-389
.. _SKB-439: https://jira.skatelescope.org/browse/SKB-439
.. _SKB-442: https://jira.skatelescope.org/browse/SKB-442

================================================================================
0.4.0
================================================================================

Release date: 2024-07-23

Release Management ticket: `REL-1590`_ 

Fixed
-----

* `SKB-259`_: low-cbf-processor tango device throwing polling error
* `SKB-286`_: LOW CBF Processor fails to load personality
* `SKB-370`_: ska-pst firmware can not be downloaded from nexus

Changed
-------

Version Updates
^^^^^^^^^^^^^^^

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-low-cbf-conn``
      - ``0.5.4``
      - ``0.5.5``
      - `Link <https://developer.skao.int/projects/ska-low-cbf-conn/en/0.5.5/README.html#changelog>`__
    * - ``ska-csp-lmc-low``
      - ``0.15.0``
      - ``0.16.2``
      - `Link <https://developer.skao.int/projects/ska-csp-lmc-low/en/0.16.2/CHANGELOG.html>`__


Other Changes
^^^^^^^^^^^^^

* Enable ``ska-low-cbf-conn`` and ``ska-low-cbf-proc`` Helm charts by default

.. _REL-1590: https://jira.skatelescope.org/browse/REL-1590
.. _SKB-259: https://jira.skatelescope.org/browse/SKB-259
.. _SKB-286: https://jira.skatelescope.org/browse/SKB-286
.. _SKB-370: https://jira.skatelescope.org/browse/SKB-370

================================================================================
0.3.1
================================================================================

Release date: 2024-06-17

Release Management ticket: `REL-1606`_ 

Changed
-------

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-csp-lmc-low``
      - ``0.13.1``
      - ``0.15.0``
      - `Link <https://developer.skao.int/projects/ska-csp-lmc-low/en/0.15.0/CHANGELOG.html>`__
    * - ``ska-low-cbf``
      - ``0.8.1``
      - ``0.9.0``
      - `Link <https://developer.skao.int/projects/ska-low-cbf/en/0.9.0/CHANGELOG.html>`__
    * - ``ska-low-cbf-proc``
      - ``0.12.0``
      - ``0.13.0``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/-/blob/0.13.0/README.md#0130>`__

.. _REL-1606: https://jira.skatelescope.org/browse/REL-1606

================================================================================
0.3.0
================================================================================

Release date: 2024-05-08

Release Management ticket: `REL-1325`_ 

Changed
-------

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-tango-base``
      - ``0.4.9``
      - ``0.4.9``
      - `Link <https://gitlab.com/ska-telescope/ska-tango-images/-/releases/0.14.15>`__
    * - ``ska-tango-util``
      - ``0.4.10``
      - ``0.4.10``
      - `Link <https://gitlab.com/ska-telescope/ska-tango-images/-/releases/0.4.14>`__
    * - ``ska-csp-lmc-low``
      - ``0.10.1``
      - ``0.13.1``
      - `Link <https://gitlab.com/ska-telescope/ska-csp-lmc-low/-/blob/0.13.1/CHANGELOG.rst>`__
    * - ``ska-low-cbf``
      - ``0.7.0``
      - ``0.8.1``
      - `Link <https://developer.skao.int/projects/ska-low-cbf/en/0.8.1/CHANGELOG.html>`__
    * - ``ska-low-cbf-conn``
      - ``0.5.2``
      - ``0.5.4``
      - `Link <https://developer.skao.int/projects/ska-low-cbf-conn/en/0.5.4/README.html#changelog>`__
    * - ``ska-low-cbf-proc``
      - ``0.11.1``
      - ``0.12.0``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/-/blob/0.12.0/README.md#anchor-0120>`__

.. _REL-1325: https://jira.skatelescope.org/browse/REL-1325

================================================================================
0.2.0
================================================================================

Release date: 2023-12-22

Release Management ticket: `REL-944`_ 

Fixed
-----

`LOW-578`_
^^^^^^^^^^

The ``hardware_connections`` table used by the LOW-CBF Allocator to map Alveos to P4 switch ports is now configurable through Helm values.
This solves the issue where a manual workaround was needed to deploy ``ska-low-csp`` with the correct mapping for the environment.

Example configuration:

.. code-block:: yaml

    ska-low-csp:
      ska-low-cbf:
        cbf:
          hardware_connections:
            - "switch=p4_01  port=5/0  speed=100  alveo=XFL1IYUNES2E"
            - "switch=p4_01  port=9/0  speed=100  alveo=XFL1EG4H5YXY"
            - "switch=p4_01  port=13/0  speed=100  alveo=XFL1XXQM0FKW"
            - "switch=p4_01  port=17/0  speed=100  alveo=XFL1BB1SEWXK"
            - "switch=p4_01  port=21/0  speed=100  alveo=XFL1SZ2IUU2I"
            - "switch=p4_01  port=25/0  speed=100  alveo=XFL121Y1KXMA"

Changed
-------

.. list-table::
    :header-rows: 1

    * - Helm Chart
      - From Version
      - To Version
      - Changelog
    * - ``ska-csp-lmc-low``
      - ``0.9.1``
      - ``0.10.1``
      - `Link <https://gitlab.com/ska-telescope/ska-csp-lmc-low/-/blob/0.10.1/CHANGELOG.rst>`__
    * - ``ska-low-cbf``
      - ``0.6.1``
      - ``0.7.0``
      - `Link <https://developer.skao.int/projects/ska-low-cbf/en/0.7.0/CHANGELOG.html>`__
    * - ``ska-low-cbf-proc``
      - ``0.10.1``
      - ``0.11.1``
      - `Link <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/-/tree/0.11.1#anchor-0111>`__
    * - ``ska-low-cbf-conn``
      - ``0.5.0``
      - ``0.5.2``
      - `Link <https://developer.skao.int/projects/ska-low-cbf-conn/en/0.5.2/README.html#changelog>`__

.. _LOW-578: https://jira.skatelescope.org/browse/LOW-578
.. _REL-944: https://jira.skatelescope.org/browse/REL-944

================================================================================
0.1.0
================================================================================

Release date: 2023-10-06

Release Management ticket: `REL-703`_ 

Added
-----

- ``ska-csp-lmc-low`` version ``0.9.1``
- ``ska-low-cbf`` version ``0.6.1``
- ``ska-low-cbf-proc`` version ``0.10.1``
- ``ska-low-cbf-conn`` version ``0.5.0``

.. _REL-703: https://jira.skatelescope.org/browse/REL-703

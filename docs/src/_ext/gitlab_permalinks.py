"""
Sphinx extension to create permalinks to GitLab.
"""

from __future__ import annotations
from typing import Literal

from docutils import nodes, utils

from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxRole
from sphinx.util.nodes import split_explicit_title
from sphinx.util.typing import ExtensionMetadata

MAPPING_CONFIG_KEY = "gitlab_permalinks_mapping"


class GitlabPermalinkRole(SphinxRole):
    """
    Custom Sphinx role that creates permalinks to files and directories in a GitLab repository.
    """

    def __init__(self, link_type: Literal["blob", "tree"]) -> None:
        super().__init__()
        self.link_type = link_type

    def run(self) -> tuple[list[nodes.Node], list[nodes.system_message]]:
        text = utils.unescape(self.text)
        has_explicit_title, title, part = split_explicit_title(text)
        repo_id, path = part.split(":")
        repo_config = self.config[MAPPING_CONFIG_KEY].get(repo_id)

        if not repo_config:
            msg = self.inliner.reporter.error(f"Missing mapping in {MAPPING_CONFIG_KEY} for {repo_id}", line=self.lineno)
            prb = self.inliner.problematic(self.text, self.rawtext, msg)
            return [prb], [msg]

        full_url = f"https://gitlab.com/{repo_config['slug']}/-/{self.link_type}/{repo_config['ref']}/{path}"
        if not has_explicit_title:
            title = full_url

        node = nodes.reference(title, title, internal=False, refuri=full_url)
        return [node], []


def setup(app: Sphinx) -> ExtensionMetadata:
    """
    Setup hook for the Sphinx extension.
    """

    app.add_role("gitlab-file-permalink", GitlabPermalinkRole(link_type="blob"))
    app.add_role("gitlab-directory-permalink", GitlabPermalinkRole(link_type="tree"))
    app.add_config_value(MAPPING_CONFIG_KEY, {}, "env")

    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys
from pathlib import Path

import yaml

PROJECT_ROOT = Path(__file__).parents[2].resolve()

sys.path.insert(0, str(PROJECT_ROOT / "src"))
sys.path.append(os.path.abspath("./_ext"))

project = "ska-low-csp"
copyright = "2022–2024 TOPIC Team"
author = "TOPIC Team"

with open(PROJECT_ROOT / "charts" / project / "Chart.yaml", "rb") as fh:
    csp_helm_chart = yaml.safe_load(fh)

with open(PROJECT_ROOT / "helmfile.d" / "helmfile.yaml", "rb") as fh:
    *_, main_helm_chart = yaml.safe_load_all(fh)

csp_version = "main"

csp_helm_chart_dependency_versions = {item["name"]: item["version"] for item in csp_helm_chart["dependencies"]}
helmfile_release_versions = {item["name"]: item.get("version", None) for item in main_helm_chart["releases"]}

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "linuxdoc.rstFlatTable",
    "sphinxemoji.sphinxemoji",
    "gitlab_permalinks",
]

source_suffix = [".rst"]
exclude_patterns = []

pygments_style = "sphinx"
sphinxemoji_style = "twemoji"

extlinks = {
    "jira": ("https://jira.skatelescope.org/browse/%s", "%s"),
}

intersphinx_mapping = {
    "ska-csp-lmc-low": (
        f"https://developer.skao.int/projects/ska-csp-lmc-low/en/{csp_helm_chart_dependency_versions['ska-csp-lmc-low']}/",
        None,
    ),
    "ska-low-cbf": (
        f"https://developer.skao.int/projects/ska-low-cbf/en/{csp_helm_chart_dependency_versions['ska-low-cbf']}/",
        None,
    ),
    "ska-low-cbf-conn": (
        f"https://developer.skao.int/projects/ska-low-cbf-conn/en/{csp_helm_chart_dependency_versions['ska-low-cbf-conn']}/",
        None,
    ),
    "ska-low-cbf-proc": (
        f"https://developer.skao.int/projects/ska-low-cbf-proc/en/{csp_helm_chart_dependency_versions['ska-low-cbf-proc']}/",
        None,
    ),
    "ska-pst": (
        f"https://developer.skao.int/projects/ska-pst/en/{csp_helm_chart_dependency_versions['ska-pst']}/",
        None,
    ),
    "ska-telmodel": ("https://developer.skao.int/projects/ska-telmodel/en/1.19.2/", None),
    "ska-dev-portal": ("https://developer.skao.int/en/latest", None),
    "ska-tango-operator": ("https://developer.skao.int/projects/ska-tango-operator/en/latest", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
}

gitlab_permalinks_mapping = {
    "ska-low-csp": {
        "slug": "ska-telescope/ska-low-csp",
        "ref": csp_version,
    },
    "ska-csp-lmc-low": {
        "slug": "ska-telescope/ska-csp-lmc-low",
        "ref": csp_helm_chart_dependency_versions["ska-csp-lmc-low"],
    },
    "ska-low-cbf": {
        "slug": "ska-telescope/low-cbf/ska-low-cbf",
        "ref": csp_helm_chart_dependency_versions["ska-low-cbf"],
    },
    "ska-low-cbf-conn": {
        "slug": "ska-telescope/low-cbf/ska-low-cbf-conn",
        "ref": csp_helm_chart_dependency_versions["ska-low-cbf-conn"],
    },
    "ska-low-cbf-proc": {
        "slug": "ska-telescope/low-cbf/ska-low-cbf-proc",
        "ref": csp_helm_chart_dependency_versions["ska-low-cbf-proc"],
    },
    "ska-pst": {
        "slug": "ska-telescope/pst/ska-pst",
        "ref": csp_helm_chart_dependency_versions["ska-pst"],
    },
    "ska-csp-testware": {
        "slug": "ska-telescope/ska-low-csp-testware",
        "ref": helmfile_release_versions["app-ska-low-csp-testware"],
    },
    "ska-tmc-low": {
        "slug": "ska-telescope/ska-tmc/ska-tmc-low-integration",
        "ref": helmfile_release_versions["app-ska-tmc-low"],
    },
    "ska-tango-taranta": {
        "slug": "ska-telescope/ska-tango-taranta-group/ska-tango-taranta-pipeline",
        "ref": helmfile_release_versions["svc-ska-tango-taranta"],
    },
    "ska-tango-tangogql": {
        "slug": "ska-telescope/ska-tango-taranta-group/ska-tango-tangogql-ariadne-pipeline",
        "ref": helmfile_release_versions["svc-ska-tango-tangogql"],
    },
    "ska-tango-archiver": {
        "slug": "ska-telescope/ska-tango-archiver",
        "ref": helmfile_release_versions["svc-ska-tango-archiver"],
    },
    "ska-tango-alarmhandler": {
        "slug": "ska-telescope/ska-tango-alarmhandler",
        "ref": helmfile_release_versions["svc-ska-tango-alarmhandler"],
    },
    "ska-tango-base": {
        "slug": "ska-telescope/ska-tango-charts",
        "ref": helmfile_release_versions["platform-ska-tango-base"],
    },
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"
html_static_path = ["_static"]

html_context = {
    "theme_logo_only": True,
    "display_gitlab": True,
    "gitlab_user": "ska-telescope",
    "gitlab_repo": project,
    "gitlab_version": "main",
    "conf_py_path": "/docs/src/",  # Path in the checkout to the docs root
    "theme_vcs_pageview_mode": "edit",
    "suffix": ".rst",
}

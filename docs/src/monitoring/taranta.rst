********************************************************************************
Taranta Dashboards
********************************************************************************

Taranta dashboards can be used to visualise the state of TANGO devices in real-time.
Dashboard configurations for the LOW-CSP sub-systems are linked below.

LOW CSP.LMC
===========

Taranta dashboards can be found :gitlab-directory-permalink:`here <ska-csp-lmc-low:resources/taranta_dashboards>`.

LOW-CBF
=======

Taranta dashboards can be found :gitlab-directory-permalink:`here <ska-low-cbf:resources/dashboards>`.

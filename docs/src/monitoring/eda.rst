********************************************************************************
Engineering Data Archive
********************************************************************************

The `Engineering Data Archive`_ (EDA) is used to archive TANGO attributes.
Its configuration is done manually by uploading config files, links to which can be found below.

LOW CSP.LMC
===========

EDA configuration can be found :gitlab-file-permalink:`here <ska-csp-lmc-low:resources/eda/eda_attr.yaml>`.

LOW-CBF
=======

Monitoring & Control
--------------------

No EDA configuration available.

Processor
---------

No EDA configuration available.

Connector
---------

EDA configuration can be found :gitlab-file-permalink:`here <ska-low-cbf-conn:resources/eda_ska_low_connector.yaml>`.

.. _Engineering Data Archive: https://confluence.skatelescope.org/display/UD/EDA+User+Guide

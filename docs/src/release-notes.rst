********************************************************************************
Release Notes
********************************************************************************

This page contains notes on the current release of ``ska-low-csp``.
Details of what has changed since the last release can be found in the :doc:`changelog`.

All SKBs can be tracked using: `TOPIC SKBs Dashboard <https://jira.skatelescope.org/secure/Dashboard.jspa>`_.

================================================================================
Compatible version information
================================================================================


This release of ``ska-low-csp`` was tested successfully in combination with the following versions:

.. list-table::
    :header-rows: 1
    :widths: 40 20 40

    * - Name
      - Version
      - Note
    * - ``ska-low-cbf-tango-cnic`` Helm Chart
      - ``0.5.2``
      -
    * - LOW-CBF CNIC firmware
      - ``0.1.13``
      - The latest release of the CNIC TANGO device is incompatible with firmware versions `0.1.12` and earlier.
    * - LOW-CBF Correlator firmware
      - ``vis:0.1.0``
      - The latest release of the `ska-low-cbf-proc` is incompatible with firmware versions `0.0.7` and earlier.
    * - PST firmware version
      - ``pst:1.0.2``
      - The latest release of the `ska-low-cbf-proc` is incompatible with firmware versions `1.0.0` and earlier.
    * - ska-low-cbf-p4``
      - ``0.5.6``
      -

SKA Telescope Model JSON schema compatibility
---------------------------------------------

Full compatibility tables can be found: :doc:`Telescope model interfaces version <ska-csp-lmc-low:telescope_model/commands_interface_version>`

This release of ``ska-low-csp`` is compatible with the following schemas from the :doc:`SKA Telescope Model <ska-telmodel:index>`:

.. flat-table::
    :header-rows: 1
    :widths: 45 15 40

    * - Schema
      - Version
      - Compatibility
    * - :doc:`ska-telmodel:schemas/csp/low/assignresources/index`
      - :doc:`3.2 <ska-telmodel:schemas/csp/low/assignresources/ska-low-csp-assignresources-3.2>`
      - |:white_check_mark:| Compatible
    * - :rspan:`1` :doc:`ska-telmodel:schemas/csp/low/configure/index`
      - :doc:`4.0 <ska-telmodel:schemas/csp/low/configure/ska-low-csp-configure-4.0>`
      - |:no_entry_sign:| Not yet supported
    * - :doc:`3.2 <ska-telmodel:schemas/csp/low/configure/ska-low-csp-configure-3.2>`
      - |:white_check_mark:| Compatible
    * - :rspan:`1` :doc:`ska-telmodel:schemas/csp/low/scan/index`
      - :doc:`4.0 <ska-telmodel:schemas/csp/low/scan/ska-low-csp-scan-4.0>`
      - |:no_entry_sign:| Not yet supported
    * - :doc:`3.2 <ska-telmodel:schemas/csp/low/scan/ska-low-csp-scan-3.2>`
      - |:white_check_mark:| Compatible
    * - :doc:`ska-telmodel:schemas/csp/low/releaseresources/index`
      - :doc:`3.2 <ska-telmodel:schemas/csp/low/releaseresources/ska-low-csp-releaseresources-3.2>`
      - |:grey_question:| Unknown [#releaseresources_not_tested]_

.. [#releaseresources_not_tested] All tests use the ``ReleaseAllResources`` command, so the JSON schema for the ``ReleaseResources`` command is currently not yet tested for compatibility.

================================================================================
PST installation
================================================================================

Resource requirements
---------------------

When deploying this release for the first time, please note that PST has some additional requirements on the servers running their components.
Consult the following resources for more information:

- :doc:`ska-pst:deployment/resource_requirements` resource requirements in the PST documentation
- JIRA ticket :jira:`STS-1251`

Workarounds
-----------

Due to some known issues, the following workarounds should be applied when deploying this release for PST to work.

Configure PST receiver MAC address
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As reported in :jira:`SPRTS-277`, the current version of the LOW-CBF beamformer firmware hard-codes the following destination MAC address in its output packets::

    94:6d:ae:92:a3:c5

The network adapter used by PST therefore needs to match this MAC address for these packets to arrive.

When using a Kubernetes ``NetworkAttachmentDefinition`` to assign a network adapter to PST, the MAC address can be configured through the annotation set on the PST Core component:

.. code-block:: yaml
    :caption: values.yaml

    ska-pst:
      core:
        podAnnotations:
          k8s.v1.cni.cncf.io/networks: '[{"name":"net-att-def-name", "namespace": "kube-system", "mac":"94:6d:ae:92:a3:c5"}]'

Configure PST receiver port
^^^^^^^^^^^^^^^^^^^^^^^^^^^

As reported in :jira:`SKB-626`, the current version of LOW-CBF hard-codes the destination port in its output packets.
The PST deployment therefore needs to be configured to use the correct port for its receiver:

.. code-block:: yaml
    :caption: values.yaml

    ska-pst:
      core:
        applications:
          recv:
            ports:
              datastream:
                port: 9510
                targetPort: 9510

================================================================================
General Notes
================================================================================

Jupyter Notebooks
-----------------

Accompanying this release of ``ska-low-csp`` is a set of Jupyter Notebooks that demonstrate several pieces of functionality.
These notebooks can be found at :gitlab-directory-permalink:`ska-low-csp:notebooks/aa05`.

Warnings and errors related to PSS
----------------------------------

As with the previous release, the logs of some of the LOW-CSP sub-systems may contain errors and/or warnings related to PSS.
These can be safely ignored, as PSS is currently not included in ``ska-low-csp``.

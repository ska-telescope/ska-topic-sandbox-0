********************************************************************************
CI/CD Pipeline
********************************************************************************

.. note:: This page is currently outdated.

The CI/CD pipeline is configured to:

* Deploy the ``ska-low-csp`` Helm chart in the CLP facility
* Run automated integration and verification tests

Helm chart deployment
=====================

The ``ska-low-csp`` Helm chart is deployed as part of an "umbrella" chart found in ``charts/test-parent/Chart.yaml``.
The umbrella chart adds components that do not belong to the System Under Test (SUT), such as Taranta.

The umbrella chart has two modes of deployment, each with their own respective values file:

#. Deployment with CBF hardware included using 
   ``charts/test-parent/values.clp.yaml``
#. Software-only deployment using ``charts/test-parent/values.clp-no-hw.yaml``

The CI/CD pipeline deploys the Helm chart with CBF hardware included to the ``ska-low-csp-baseline`` Kubernetes namespace.
These deployments are always made from the ``main`` GitLab branch.

Integration and verification tests
==================================

The automated integration and verification tests are implemented in the `ska-low-csp-test`_ repository.
The CI/CD pipeline triggers these tests automatically after deployment.

.. _ska-low-csp-test: https://gitlab.com/ska-telescope/ska-low-csp-test/

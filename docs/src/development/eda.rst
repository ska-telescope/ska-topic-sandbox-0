********************************************************************************
Engineering Data Archive (EDA)
********************************************************************************

This guide explains how to interact with the Engineering Data Archive (EDA).
It is meant to complement the `EDA User Guide`_, so please read that first.

.. note::

    Interacting with the EDA requires an active VPN connection to the Digital Signal PSI.
    Refer to the `VPN Guide`_ to get it set up.

================================================================================
Configuration of the EDA
================================================================================

The EDA is configured through a web UI called the *configurator*.
The link to the EDA configurator is http://k8s.clp.skao.int/ska-low-csp-dev/configurator/.

Configuration is done by uploading YAML configuration files through the web UI.
For links to configuration files, refer to :doc:`../monitoring/eda`.

After uploading a configuration file, the page should list all added attributes in a table.

.. note::

    It may take some time after submitting the configuration file for the table to appear.

After the EDA has been configured with a list of TANGO attributes to archive,
this configuration is propagated to a TANGO device ``low-eda/es/01``.
This device can be queried in `Taranta`_ to verify that the attributes are correctly being pulled.

.. _EDA User Guide: https://confluence.skatelescope.org/display/UD/EDA+User+Guide
.. _Taranta: http://k8s.clp.skao.int/ska-low-csp-dev/taranta/devices/low-eda/es/01/
.. _VPN Guide: https://confluence.skatelescope.org/pages/viewpage.action?pageId=200026561

********************************************************************************
Using the Makefile
********************************************************************************

The ``ska-low-csp`` repository relies on ``make`` to automate most development tasks like building and linting source code, running tests and deployments.

The ``make`` targets are defined in the ``Makefile``. This file includes targets from `ska-cicd-makefile`_, refer to their README for more information on how this works.

================================================================================
Variables
================================================================================

The ``Makefile`` contains default values for a lot of variables, which can be overridden in two ways.

--------------------------------------------------------------------------------
Overriding from the command line
--------------------------------------------------------------------------------

To override a variable from the command line, use the following format:

.. code-block:: console

    make <target> VARIABLE=value

For example, this command installs the Helm chart in Kubernetes, but overrides the Kubernetes namespace to ``testing123``:

.. code-block:: console

    make k8s-install-chart KUBE_NAMESPACE=testing123

.. _makefile-privaterules:

--------------------------------------------------------------------------------
Overriding from ``PrivateRules.mak``
--------------------------------------------------------------------------------

The ``Makefile`` is configured to include a file called ``PrivateRules.mak``, which is ignored by Git and can therefore contain user-specific overrides.

This is useful for variables that you would otherwise have to override manually each time you run a ``make`` command.

For example, to override the ``KUBE_NAMESPACE`` variable to change the default namespace used to install the Helm chart:

.. code-block:: make
    :caption: PrivateRules.mak

    KUBE_NAMESPACE = testing123

.. _ska-cicd-makefile: https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile

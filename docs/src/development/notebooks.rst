********************************************************************************
Jupyter Notebooks
********************************************************************************

Coding guidelines
=================

All Jupyter Notebooks in ``ska-low-csp`` follows the SKAO Jupyter Notebook coding guidelines.
For more information, refer to :doc:`ska-dev-portal:tools/codeguides/jupyter-notebook-codeguide` on the SKAO Developer Portal.

Code formatting and linting
===========================

All Jupyter Notebooks in ``ska-low-csp`` are formatted and linted using the SKAO standards, unless there is a *very* good reason to do otherwise.

The GitLab CI/CD pipeline configuration includes jobs to check the notebooks for formatting and linting errors, and will fail if any errors are found. 

It is recommended to run these tools prior to committing your changes to speed up your development process.
Use the following commands to format and then lint all notebooks in the repository:

.. code-block:: console

    make notebook-format
    make notebook-lint

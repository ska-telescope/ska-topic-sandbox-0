********************************************************************************
SKA LOW Central Signal Processor
********************************************************************************

The SKA LOW Central Signal Processor (LOW-CSP) is responsible for converting SKA1 LOW station data in to science data products.
It is composed of multiple sub-systems, see :doc:`architecture/index`.

The `ska-low-csp`_ repository contains Helm Charts that package the LOW-CSP so that it can be installed as a single system.

Support
=======

For support, please see the `#help-low-csp Slack channel`_.

Contents
========

.. toctree::
  :maxdepth: 1
  :caption: About

  release-notes
  changelog

.. toctree::
  :maxdepth: 1
  :caption: Architecture

  architecture/index

.. toctree::
  :maxdepth: 1
  :caption: Deployment

  deployment/helm-chart
  deployment/test-env-composition

.. toctree::
  :maxdepth: 1
  :caption: Monitoring

  monitoring/eda
  monitoring/taranta

.. toctree::
  :maxdepth: 1
  :caption: Development

  development/getting-started
  development/makefile
  development/notebooks
  development/documentation
  development/cicd-pipeline
  development/eda

.. _ska-low-csp: https://gitlab.com/ska-telescope/ska-low-csp
.. _#help-low-csp Slack channel: https://skao.slack.com/archives/C06BJJ2RGBB

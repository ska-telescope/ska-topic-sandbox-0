********************************************************************************
Test Environment Composition 
********************************************************************************
Besides our main integration system, LOW-CSP, the test environment consists of several other components needed to do testing on the system. This includes some testing tools, global SKA components, data generation tools, etc.

The full list of components can be found at :gitlab-file-permalink:`helmfile.d/helmfile.yaml <ska-low-csp:helmfile.d/helmfile.yaml>`.


================================================================================
CSP Testware
================================================================================
For more info refer to :gitlab-file-permalink:`SKA LOW CSP Testware Helm Chart <ska-csp-testware:charts/ska-low-csp-testware>`

================================================================================
TMC system
================================================================================

The TMC system is not part of LOW-CSP but is still deployed as part of the CSP deployment to be used in combination with a real integrated CSP sub-system.

The default chart values can be found in :gitlab-file-permalink:`LOW-TMC Helm values <ska-tmc-low:charts/ska-tmc-low/values.yaml>`

--------------------------------------------------------------------------------
Sub-Systems
--------------------------------------------------------------------------------
Several sub-systems are needed for TMC to funcion completely. Each of these sub-systems can be individually configured as a mock instance in the Helm chart.

.. list-table::
    :header-rows: 1
    :widths: 20 60 20
        
    * - Sub-system
      - Description
      - Mocked
    * - MCCS
      - Monitor, Control and Calibration Subsystem. The sub-system responsible for providing CSP with data.
      - Yes
    * - CSP
      - Central Signal Processor. The sub-system which is deployed as described in :doc:`helm-chart`.
      - No
    * - SDP
      - Science Data Processor. The sub-system receiving data from CSP for further processing.
      - Yes

--------------------------------------------------------------------------------
Configuration
--------------------------------------------------------------------------------
The Helm values that are used to configure the ``ska-low-tmc`` can be found at :gitlab-file-permalink:`helmfile.d/values/ska-tmc-low.yaml <ska-low-csp:helmfile.d/values/ska-tmc-low.yaml>`.

================================================================================
SKA Tango Charts
================================================================================

--------------------------------------------------------------------------------
SKA Tango Taranta
--------------------------------------------------------------------------------
For more info refer to :gitlab-file-permalink:`SKA Tango Taranta Helm Chart <ska-tango-taranta:charts/ska-tango-taranta>`

--------------------------------------------------------------------------------
SKA Tango TangoGQL
--------------------------------------------------------------------------------
For more info refer to :gitlab-file-permalink:`SKA TangoGQL Ariadne Helm Chart <ska-tango-tangogql:charts/ska-tango-tangogql-ariadne>`

--------------------------------------------------------------------------------
SKA Tango Archiver
--------------------------------------------------------------------------------
For more info refer to :gitlab-file-permalink:`SKA Tango Archiver Helm Chart <ska-tango-archiver:charts/ska-tango-archiver>`

--------------------------------------------------------------------------------
SKA Tango Alarmhandler
--------------------------------------------------------------------------------
For more info refer to :gitlab-file-permalink:`SKA Tango Alarmhandler Helm Chart <ska-tango-alarmhandler:charts/ska-tango-alarmhandler>`

--------------------------------------------------------------------------------
SKA Tango Base
--------------------------------------------------------------------------------
For more info refer to :gitlab-file-permalink:`SKA Tango Base Helm Chart <ska-tango-base:charts/ska-tango-base>`

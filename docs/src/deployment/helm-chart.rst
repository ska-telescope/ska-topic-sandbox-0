********************************************************************************
LOW-CSP Helm Chart
********************************************************************************

The deployment of LOW-CSP is done using the ``ska-low-csp`` Helm chart.
This chart integrates the components of the LOW-CSP LMC, LOW-CBF, PST and PSS sub-systems.
These components are included as sub-charts of the ``ska-low-csp`` chart.

.. note::

    The PSS sub-system is currently not yet included in LOW-CSP.

================================================================================
Sub-system Components
================================================================================

The LOW-CSP Helm chart contains the following sub-system charts:

.. list-table::
    :header-rows: 1
    :widths: 20 25 55

    * - Sub-system
      - Chart
      - Description
    * - LOW-CSP LMC
      - ``ska-csp-lmc-low``
      - This chart contains the ``LowCspController`` and ``LowCspSubarray`` TANGO devices, which are used for Monitoring & Control of the LOW-CSP system.
        Documentation: :doc:`ska-csp-lmc-low:index`.
    * - LOW-CBF
      - ``ska-low-cbf``
      - This chart contains the ``LowCbfController``, ``LowCbfSubarray`` and ``LowCbfAllocator`` TANGO devices, which are used for Monitoring & Control of the LOW-CBF sub-system.
        Documentation: :doc:`ska-low-cbf:index`.
    * - LOW-CBF
      - ``ska-low-cbf-proc``
      - This chart contains the ``LowCbfProcessor`` TANGO device, which controls the FPGAs used by LOW-CBF to perform their correlation and beamforming functionality.
        Documentation: :doc:`ska-low-cbf-proc:index`.
    * - LOW-CBF
      - ``ska-low-cbf-conn``
      - This chart contains the ``LowCbfConnector`` TANGO device, which controls the P4 switches used to route science data from SPS through the LOW-CSP into SDP.
        Documentation: :doc:`ska-low-cbf-conn:index`.
    * - PST
      - ``ska-pst``
      - This chart contains all PST sub-system components. Documentation: :doc:`ska-pst:index`.

================================================================================
Configuration
================================================================================

The following Helm values are used to configure the ``ska-low-csp`` Helm chart.
The default values can be found in :gitlab-file-permalink:`charts/ska-low-csp/values.yaml <ska-low-csp:charts/ska-low-csp/values.yaml>`.

.. list-table::
    :header-rows: 1
    :widths: 30 70

    * - Parameter
      - Description
    * - ``ska-csp-lmc-low``
      - Configuration of the ``ska-csp-lmc-low`` Helm chart.
        For an overview of possible values, refer to the :gitlab-file-permalink:`LOW-CSP.LMC Helm values <ska-csp-lmc-low:charts/ska-csp-lmc-low/values.yaml>`.
    * - ``ska-low-cbf``
      - Configuration of the ``ska-low-cbf`` Helm chart.
        For an overview of possible values, refer to the :gitlab-file-permalink:`LOW-CBF Helm values <ska-low-cbf:charts/ska-low-cbf/values.yaml>`.
    * - ``ska-low-cbf-conn``
      - Configuration of the ``ska-low-cbf-conn`` Helm chart.
        For an overview of possible values, refer to the :gitlab-file-permalink:`LOW-CBF Connector Helm values <ska-low-cbf-conn:charts/ska-low-cbf-conn/values.yaml>`.
    * - ``ska-low-cbf-conn.enabled``
      - Disables the ``ska-low-cbf-conn`` Helm chart, to support deploying to environments without the required LOW-CBF hardware.
    * - ``ska-low-cbf-proc``
      - Configuration of the ``ska-low-cbf-proc`` Helm chart.
        For an overview of possible values, refer to the :gitlab-file-permalink:`LOW-CBF Processor Helm values <ska-low-cbf-proc:charts/ska-low-cbf-proc/values.yaml>`.
    * - ``ska-low-cbf-proc.enabled``
      - Disables the ``ska-low-cbf-proc`` Helm chart, to support deploying to environments without the required LOW-CBF hardware.
    * - ``ska-pst``
      - Configuration of the ``ska-csp-lmc-low`` Helm chart.
        Refer to :doc:`ska-pst:configuration/helm`.
    * - ``ska-pst.enabled``
      - Disables the ``ska-pst`` Helm chart, for environments where PST is not explicitly desired.

For an example of how the ``ska-low-csp`` Helm chart is deployed to Digital Signal PSI, refer to :gitlab-file-permalink:`helmfile.d/values/ska-low-csp.yaml <ska-low-csp:helmfile.d/values/ska-low-csp.yaml>`.

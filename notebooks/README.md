# LOW-CSP Notebooks

This directory contains Jupyter Notebooks to complement the LOW-CSP user documentation.

Notebooks in this directory are required to be allowed to run stand-alone on any Jupyter server instance.
This means that they should not assume the presence of any Python packages outside of the standard library,
and that they cannot rely on any helper functionality that is not installable through `pip` - either from PyPi or CAR.
